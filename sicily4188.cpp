// 题目分析:
//用了个不怎么的技巧,将每次除以2的余数放进堆栈,刚好先进后出的原则,最后弹出来就成了.

//题目网址:http://soj.me/4188 

#include<iostream>
#include<stack>
using namespace std;
int main()
{
    int t;
    cin >> t;
    int dec;
    stack<int> bin;
    while(t--){
        cin >> dec;
        if(dec == 0){
            cout << '0' << endl;
            continue;
        }
        while(dec != 0){
            bin.push(dec % 2);
            dec /= 2;
        }
        while(!bin.empty()){
            cout << bin.top();
            bin.pop();
        }
        cout << endl;
    }
    return 0;
}                                 